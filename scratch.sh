#!/usr/bin/env bash

#---------------------------------------------
# Date: 14/11/2019
# Code by: David Encinas
# Modified by: David Encinas
# XbO Link Reference: https://confluence.si.orange.es/display/ORXSAL/Install+Vlocity
#---------------------------------------------

#OS
echo "$OSTYPE"
echo

CREATE_SCRATCH="false"
DEPLOY_PACKAGE="false"
LOAD_SF_DATA="false"
VALIDATE="false"
ERROR="false"

INPUT1=$1
SF_DevHub_USERNAME=$2

i=0

# Load Arguments
for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)

    case "$KEY" in
            -c)         CREATE_SCRATCH=true ;;
            -dp)        DEPLOY_PACKAGE=true ;;
            -data)      LOAD_SF_DATA=true ;;
            -v)   VALIDATE=true ;;
            *)
    esac
done

# Show Arguments in the screen
echo -e "\e[32m####### The parameters are the right ones"
echo -e "\e[39m"

echo "createScratch: $CREATE_SCRATCH"
echo "deployPackage: $DEPLOY_PACKAGE"
echo "salesforceData: $LOAD_SF_DATA"
echo -e "\e[39m"

# Validate the base parameters
if [[  $INPUT1 &&  $SF_DevHub_USERNAME ]]
then
   
    SF_NEW_SCRATCH_ORG_NAME="$INPUT1"

    #Delete previous Logs
    LOG_FILE="${SF_NEW_SCRATCH_ORG_NAME}.log"
    rm -f "$LOG_FILE"

    # Create Scratch
    if [ $CREATE_SCRATCH = "true" ]
    then
        echo "####### Creating New Scratch Org"
        CMD_NEW_SCRATCH_ORG="sfdx force:org:create -f config/project-scratch-def.json -a $SF_NEW_SCRATCH_ORG_NAME orgName=$SF_NEW_SCRATCH_ORG_NAME -d 29 -u $SF_DevHub_USERNAME --json"

        echo "CMD_NEW_SCRATCH_ORG: $CMD_NEW_SCRATCH_ORG"
        SCRATCH_ORG=$( $CMD_NEW_SCRATCH_ORG | tee /dev/tty)

        echo "Extract User name"
        SF_USERNAME=$(echo "$SCRATCH_ORG" | jq -r '. | .result.username')
        echo "SF_USERNAME: $SF_USERNAME"

        export SF_USERNAME

        echo "####### New Scratch Org Information"
        sfdx force:org:display -u "$SF_NEW_SCRATCH_ORG_NAME" --json

        sfdx force:user:password:generate -u "$SF_NEW_SCRATCH_ORG_NAME"

    else
        #Get Scratch Information
        echo -e "\e[31m####### NOT Creating New Scratch Org"
        echo -e "\e[39m"

        CMD_DISPLAY_SCRATCH_ORG="sfdx force:org:display -u $SF_NEW_SCRATCH_ORG_NAME  --json"
        DISPLAY_SCRATCH_RESULT=$( $CMD_DISPLAY_SCRATCH_ORG | tee /dev/tty)
        
        SF_USERNAME=$(echo "$DISPLAY_SCRATCH_RESULT" | jq -r '. | .result.username')
        export SF_USERNAME
        echo -e "\e[39m"
    fi

    # Deploy Package
    if [ $DEPLOY_PACKAGE == 'true' ]
    then

        echo -e "\e[32m####### Deploying Custom Role"
        sfdx force:source:deploy -p "force-app/main/default/roles" -u "$SF_NEW_SCRATCH_ORG_NAME"

        #Get Custom Role
        echo -e "\e[32m####### Get Custom Role Id"
        SF_CUSTOM_ROLE_ID=$(sfdx force:data:soql:query -q "SELECT Id, Name FROM UserRole WHERE Name = 'Custom Role'" -u $SF_NEW_SCRATCH_ORG_NAME --json | jq '.result.records[0].Id')
        echo -e $SF_CUSTOM_ROLE_ID
        #Get Standar Profile
        echo -e "\e[32m####### Get Standard Profile Id"
        SF_STANDARD_PROFILE_ID=$(sfdx force:data:soql:query -q "SELECT Id, Name FROM Profile WHERE Name = 'Standard Platform User'" -u $SF_NEW_SCRATCH_ORG_NAME --json | jq '.result.records[0].Id')
        echo -e $SF_STANDARD_PROFILE_ID

        #Create Users
        echo -e "\e[32m####### Create Agent Users"
        sfdx force:data:record:create -s User -v "UserRoleId=$SF_CUSTOM_ROLE_ID FirstName='Agent' LastName='Loo' Email='david.encinas.gutierrez@gmail.com' UserName='david.encinas.gutierrez.agentloo@$SF_NEW_SCRATCH_ORG_NAME.com' TimeZoneSidKey='Europe/Paris' Alias='agentLow' IsActive=true LocaleSidKey='en_US' EmailEncodingKey='UTF-8' LanguageLocaleKey='en_US' profileId=$SF_STANDARD_PROFILE_ID" -u "$SF_NEW_SCRATCH_ORG_NAME" 
        sfdx force:data:record:create -s User -v "UserRoleId=$SF_CUSTOM_ROLE_ID FirstName='Agent' LastName='Med' Email='david.encinas.gutierrez@gmail.com' UserName='david.encinas.gutierrez.agentmed@$SF_NEW_SCRATCH_ORG_NAME.com' TimeZoneSidKey='Europe/Paris' Alias='agentMed' IsActive=true LocaleSidKey='en_US' EmailEncodingKey='UTF-8' LanguageLocaleKey='en_US' profileId=$SF_STANDARD_PROFILE_ID" -u "$SF_NEW_SCRATCH_ORG_NAME" 
        sfdx force:data:record:create -s User -v "UserRoleId=$SF_CUSTOM_ROLE_ID FirstName='Agent' LastName='Hij' Email='david.encinas.gutierrez@gmail.com' UserName='david.encinas.gutierrez.agenthij@$SF_NEW_SCRATCH_ORG_NAME.com' TimeZoneSidKey='Europe/Paris' Alias='agentHij' IsActive=true LocaleSidKey='en_US' EmailEncodingKey='UTF-8' LanguageLocaleKey='en_US' profileId=$SF_STANDARD_PROFILE_ID" -u "$SF_NEW_SCRATCH_ORG_NAME" 

        ## Change the User If to deploy in a Scratch dynamically
        echo -e "\e[32m####### Get Agent Low Id"
        SF_AGENT_LOW_ID=$(sfdx force:data:soql:query -q "SELECT Id FROM User WHERE UserName='david.encinas.gutierrez.agentloo@$SF_NEW_SCRATCH_ORG_NAME.com'" -u $SF_NEW_SCRATCH_ORG_NAME --json | jq --raw-output '.result.records[0].Id')
        echo -e $SF_AGENT_LOW_ID
        sed -i "s/0051X0000062tqXQAQ/$SF_AGENT_LOW_ID/" force-app/main/default/flows/CW_AccountWorkFlow.flow-meta.xml
        sed -i "s/0051X0000062tqXQAQ/$SF_AGENT_LOW_ID/" force-app/main/default/flows/CW_ContactWorkFlow.flow-meta.xml
        
        echo -e "\e[32m####### Get Agent Medium Id"
        SF_AGENT_MEDIUM_ID=$(sfdx force:data:soql:query -q "SELECT Id FROM User WHERE UserName='david.encinas.gutierrez.agentmed@$SF_NEW_SCRATCH_ORG_NAME.com'" -u $SF_NEW_SCRATCH_ORG_NAME --json | jq --raw-output '.result.records[0].Id')
        echo -e $SF_AGENT_MEDIUM_ID
        sed -i "s/0051X0000062tqcQAA/$SF_AGENT_MEDIUM_ID/" force-app/main/default/flows/CW_AccountWorkFlow.flow-meta.xml
        sed -i "s/0051X0000062tqcQAA/$SF_AGENT_MEDIUM_ID/" force-app/main/default/flows/CW_ContactWorkFlow.flow-meta.xml
        
        echo -e "\e[32m####### Get Agent High Id"
        SF_AGENT_HIGH_ID=$(sfdx force:data:soql:query -q "SELECT Id FROM User WHERE UserName='david.encinas.gutierrez.agenthij@$SF_NEW_SCRATCH_ORG_NAME.com'" -u $SF_NEW_SCRATCH_ORG_NAME --json | jq --raw-output '.result.records[0].Id')
        echo -e $SF_AGENT_HIGH_ID
        sed -i "s/0051X0000062tqhQAA/$SF_AGENT_HIGH_ID/" force-app/main/default/flows/CW_AccountWorkFlow.flow-meta.xml
        sed -i "s/0051X0000062tqhQAA/$SF_AGENT_HIGH_ID/" force-app/main/default/flows/CW_ContactWorkFlow.flow-meta.xml

        echo -e "\e[32m####### Deploying SF Code Base"
        sfdx force:source:push -f -u "$SF_NEW_SCRATCH_ORG_NAME" --json

        ## Change the User If to deploy in a Scratch dynamically - REVERT
        sed -i "s/$SF_AGENT_LOW_ID/0051X0000062tqXQAQ/" force-app/main/default/flows/CW_AccountWorkFlow.flow-meta.xml
        sed -i "s/$SF_AGENT_MEDIUM_ID/0051X0000062tqcQAA/" force-app/main/default/flows/CW_AccountWorkFlow.flow-meta.xml
        sed -i "s/$SF_AGENT_HIGH_ID/0051X0000062tqhQAA/" force-app/main/default/flows/CW_AccountWorkFlow.flow-meta.xml
        sed -i "s/$SF_AGENT_LOW_ID/0051X0000062tqXQAQ/" force-app/main/default/flows/CW_ContactWorkFlow.flow-meta.xml
        sed -i "s/$SF_AGENT_MEDIUM_ID/0051X0000062tqcQAA/" force-app/main/default/flows/CW_ContactWorkFlow.flow-meta.xml
        sed -i "s/$SF_AGENT_HIGH_ID/0051X0000062tqhQAA/" force-app/main/default/flows/CW_ContactWorkFlow.flow-meta.xml

        echo -e "\e[39m"
    else
        echo -e "\e[31m####### NOT Deploying SF Code Base"
        
        echo -e "\e[39m"
    fi

    # Load Data
    if [ $LOAD_SF_DATA == 'true' ]
    then
        #Get Custom Role
        echo -e "\e[32m####### Loading Data"
         
        echo -e "\e[39m"
    else
        echo -e "\e[31m####### NOT Loading Data"

        echo -e "\e[39m"
    fi

    # VALIDATE
    if [ $VALIDATE == 'true' ]
    then
        ####### Deploying SFDX Scanner-plugin"
        echo -e "\e[32m####### Installing sfdx Scanner"
        echo 'y' | sfdx plugins:install @salesforce/sfdx-scanner
        
        echo -e "\e[32m####### Running sfdx Scanner"
        sfdx scanner:run --target ".\**\core\**\*.cls,.\**\controller\**\*.cls,.\**\domain\**\*.cls,.\**\service\**\*.cls,.\**\selector\**\*.cls,.\**\dto\**\*.cls,.\**\rest\**\*.cls,!.\**\test\**\*.cls,!.\**\lang\**\*.cls,!.\fflib\**\*.cls" -c "Performance, Security, Best Practices, Code Style, Error Prone, Documentation, ECMAScript 6, Possible Errors, Variables, LWC"

        echo -e "\e[39m"
    fi
else
   echo -e "\e[31m####### The parameters are NOT right: scratch.sh {NAME_SCRATCH} {DEBHUB_USERNAME} -c -dv -dp -data -v"
fi