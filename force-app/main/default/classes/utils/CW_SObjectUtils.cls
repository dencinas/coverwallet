/**
* @author David Encinas david.encinas.gutierrez@gmail.com
* @date 2020-12-21
* @group Utils
* @description Util methods class to SObject
*/
public with sharing class CW_SObjectUtils {

    /**
     * @description Tries to get the given field value from the given record. Throws exception if the field doesn't exist.
     * @param fieldName the name of the field.
     * @param record the record to get the value from.
     * @return the value of the field. Any kind of field will be casted to String.
     */
    public static String getSObjectInnerFieldValue(String innerObject , String innerFieldName, SObject record) {
        try {
            Object value;
            if (innerObject!=null){
                value = record.getSObject(innerObject).get(innerFieldName); 
            }
            else{
                value = record.get(innerFieldName); 
            }

            if (value == null) return null;

            return String.valueOf(value);
        }
        catch (System.SObjectException soe) {
            throw new CW_Exceptions.SystemException(soe.getMessage());
        }
    }

    /**
     * @description It groups into a map the given record in the given main map using the given main map key.
     * @param mainMapKey the key of the main map.
     * @param innerMapKey the key of the secondary map.
     * @param record the record to group.
     * @param mainMap the main map.
     */
    private static void groupByMap(String mainMapKey, String innerMapKey, Object record, Map<String, Map<String, Object>> mainMap) {
        if (mainMap == null) throw new CW_Exceptions.SystemException('"mainMap" is null.');

        Map<String, Object> innerMap;
        if (mainMap.containsKey(mainMapKey)) {
            innerMap = mainMap.get(mainMapKey);
        }
        else {
            innerMap = (record instanceof SObject) ? new Map<String, SObject>() : new Map<String, Object>();
            mainMap.put(mainMapKey, innerMap);
        }
        innerMap.put(innerMapKey, record);
    }

    /**
     * @description Returns a map with the given records group by the given field. Uses a map to group all records with the same field value.
     * @param mainKeyFieldName the field to group the key of the main map.
     * @param innerKeyFieldName the field to group the key of the secondary map (the one on the value of the main map).
     * @param records the records to group.
     * @return a map with the given records group by the given field.
     */
    public static Map<String, Map<String, SObject>> groupRecords(String mainKeyObjectName, String mainKeyFieldName, String innerKeyObjectName, String innerKeyFieldName, List<SObject> records) {
        if (mainKeyFieldName == null) throw new CW_Exceptions.SystemException('"mainKeyFieldName" is null.');
        if (innerKeyFieldName == null) throw new CW_Exceptions.SystemException('"innerKeyFieldName" is null.');
        if (records == null) throw new CW_Exceptions.SystemException('"records" is null.');

        Map<String, Map<String, SObject>> mapAux = new Map<String, Map<String, SObject>>();
        for (SObject record : records) {
            if (record == null) continue;

            String mainMapKey = getSObjectInnerFieldValue(mainKeyObjectName, mainKeyFieldName, record);
            String innerMapKey = getSObjectInnerFieldValue(innerKeyObjectName, innerKeyFieldName, record);

            groupByMap(mainMapKey, innerMapKey, record, mapAux);
        }

        return mapAux;
    }
}
