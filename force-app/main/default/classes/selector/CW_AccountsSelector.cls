/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-20
 * @group Selector
 * @description Apex Implementation which has the Selector Layer for Customer Info
 */
public with sharing class CW_AccountsSelector {
    /**
     * @description CW_AccountsSelector Constructor
     */
    public CW_AccountsSelector() {

    }

    /**
     * @description Selector method to return the list of Contacts based on the Account External Id
     * @param externalIds Set of External Ids of Accounts related to the Contact
     * @return List<Contact> List of Contacts
     */
    public List<Account> getByCWExternalId(Set<String> externalIds){
        List<Account> accounts = new List<Account>();
        if (Account.SObjectType.getDescribe().isAccessible()){
            accounts = [SELECT Id, Name, NumberOfEmployees, AnnualRevenue, CW_Id__c, Owner.Name, CW_Priority__c FROM Account WHERE CW_Id__c =: externalIds];
        }
        return accounts;
    }
}
