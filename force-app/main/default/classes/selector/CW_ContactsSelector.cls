/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-20
 * @group Selector
 * @description Apex Implementation which has the Selector Layer for Customer Info
 */
public with sharing class CW_ContactsSelector {
    /**
     * @description CW_ContactSelector Constructor
     */
    public CW_ContactsSelector() {

    }

    /**
     * @description Selector method to return the list of Contacts based on the Account External Id
     * @param externalIds Set of External Ids of Accounts related to the Contact
     * @return List<Contact> List of Contacts
     */
    public List<Contact> getContactByAccountId(Set<String> externalIds){
        List<Contact> contacts = new List<Contact>();
        if (Contact.SObjectType.getDescribe().isAccessible() && Account.SObjectType.getDescribe().isAccessible()){
            contacts = [SELECT Id, AccountId, Account.CW_Id__c, Email, FirstName, LastName, Owner.Name FROM Contact WHERE Contact.Account.CW_Id__c =: externalIds ORDER BY AccountId, Email];
        }
        return contacts;
    }
}
