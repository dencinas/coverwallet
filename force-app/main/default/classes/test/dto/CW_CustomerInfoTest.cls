/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-22
 * @group DTO Test
 * @description Apex Test Class reakted with CW_CustomerInfo class
 */
@IsTest
public class CW_CustomerInfoTest {

    /**
     * @description Test Method for CW_CustomerInfo
     */
    static testMethod void testParse() {
        String json = '{'+
        '    \"accounts\": [{'+
        '        \"uuid\": \"123456-123456\",'+
        '        \"company_name\": \"Acme Corp.\",'+
        '        \"annual_revenue\": 120000,'+
        '        \"number_employees\": 8,'+
        '        \"contacts\": [{'+
        '            \"first_name\": \"John\",'+
        '            \"last_name\": \"Smith\",'+
        '            \"email\": \"john@acme.com\"'+
        '        }, {'+
        '            \"first_name\": \"Maria\",'+
        '            \"last_name\": \"Doe\",'+
        '            \"email\": \"maria@acme.com\"'+
        '        }]'+
        '    }]'+
        '}';
        CW_CustomerInfo r = CW_CustomerInfo.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        CW_CustomerInfo.Contact objContacts = new CW_CustomerInfo.Contact(System.JSON.createParser(json));
        System.assert(objContacts != null);
        System.assert(objContacts.first_name == null);
        System.assert(objContacts.last_name == null);
        System.assert(objContacts.email == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        CW_CustomerInfo objCW_CustomerInfo = new CW_CustomerInfo(System.JSON.createParser(json));
        System.assert(objCW_CustomerInfo != null);
        System.assert(objCW_CustomerInfo.accounts == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        CW_CustomerInfo.Account objAccounts = new CW_CustomerInfo.Account(System.JSON.createParser(json));
        System.assert(objAccounts != null);
        System.assert(objAccounts.uuid == null);
        System.assert(objAccounts.company_name == null);
        System.assert(objAccounts.annual_revenue == null);
        System.assert(objAccounts.number_employees == null);
        System.assert(objAccounts.contacts == null);
    }
}