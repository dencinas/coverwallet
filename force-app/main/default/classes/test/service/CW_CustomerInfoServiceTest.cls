/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-22
 * @group Service Test
 * @description Apex Test Class reakted with CW_CustomerInfoService class
 */
@IsTest
public class CW_CustomerInfoServiceTest {

    /**
     * @descript Account Name Constant
     */
    final static String ACCOUNT_NAME = 'Account Name Corp.';

    /**
     * @descript Account Name Constant
     */
    final static String ACCOUNT_2_NAME = 'Acme Corp.';

    /**
     * @descript Account Number of Employees Constant
     */
    final static Integer ACCOUNT_EMPLOYEES = 50;

    /**
     * @descript Account Annual Revenue Constant
     */
    final static Integer ACCOUNT_REVENUE_LOW = 50000;

    /**
     * @descript Account Annual Revenue Constant
     */
    final static Integer ACCOUNT_REVENUE_MEDIUM = 50001;

    /**
     * @descript Account Annual Revenue Constant
     */
    final static Integer ACCOUNT_REVENUE_HIGH = 120000;

    /**
     * @descript Account Annual Revenue Constant
     */
    final static String ACCOUNT_EXTERNAL_ID = '111111-111111';

    /**
     * @descript Account Annual Revenue Constant
     */
    final static String ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT = '123456-123456';

    /**
     * @descript Account Priority High Constant
     */
    final static String ACCOUNT_PRIORITY_HIGH = 'High';

    /**
     * @descript Account Priority Low Constant
     */
    final static String ACCOUNT_PRIORITY_LOW = 'Low';

    /**
     * @descript Account Priority Medium Constant
     */
    final static String ACCOUNT_PRIORITY_MEDIUM = 'Medium';

    /**
     * @descript Agent User Name for High Priority
     */
    final static String AGENT_NAME_PRIORITY_HIGH = 'Agent Hij';

    /**
     * @descript Agent User Name for Low Priority
     */
    final static String AGENT_NAME_PRIORITY_LOW = 'Agent Loo';

    /**
     * @descript Agent User Name for Medium Priority
     */
    final static String AGENT_NAME_PRIORITY_MEDIUM = 'Agent Med';

    /**
     * @description SetUp Test
     */
    @TestSetup
    static void makeData(){
        Account acc = new Account(Name=ACCOUNT_NAME, AnnualRevenue=ACCOUNT_REVENUE_LOW, NumberOfEmployees=ACCOUNT_EMPLOYEES, CW_Id__c=ACCOUNT_EXTERNAL_ID);

    }

    @IsTest
    static void updateInsertCustomerInfo(){
        //START Test Creation of Accounts
        String json = '{'+
        '    \"accounts\": [{'+
        '        \"uuid\": \"' + ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT + '\",'+
        '        \"company_name\": \"' + ACCOUNT_2_NAME + '\",'+
        '        \"annual_revenue\": ' + ACCOUNT_REVENUE_HIGH + ','+
        '        \"number_employees\": 8'+
        '    }]'+
        '}';
        CW_CustomerInfo customInfo = CW_CustomerInfo.parse(json);

        CW_CustomerInfoService service = new CW_CustomerInfoService();
        String responseString = service.updateInsertCustomerInfo(customInfo);
        CW_AbstractRestService.Response response = (CW_AbstractRestService.Response)System.JSON.deserialize(responseString, CW_AbstractRestService.Response.class);

        //Check the Response Code
        System.assertEquals(response.responseCode,CW_AbstractRestService.SUCCESS_CODE_200,'The expected response code is not matching');

        //Check that the records has been created and the Process Builder and Formula Fields are working fine
        CW_AccountsSelector accountsSelector = new CW_AccountsSelector();
        CW_ContactsSelector contactsSelector = new CW_ContactsSelector();
        List<Account> accounts = accountsSelector.getByCWExternalId(new Set<String>{ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT});
        List<Contact> contacts = contactsSelector.getContactByAccountId(new Set<String>{ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT});
        System.assertEquals(1, accounts.size(), 'The Number of Accounts is not matching');
        System.assertEquals(ACCOUNT_PRIORITY_HIGH, accounts.get(0).CW_Priority__c, 'The Account Priority is not matching');
        System.assertEquals(AGENT_NAME_PRIORITY_HIGH, accounts.get(0).Owner.Name, 'The Account Owner is not matching');
        System.assertEquals(0, contacts.size(), 'The Number of Contacts related with the Account is not matching');

        //END Test Creation of Accounts

        //START Test Creation of Contacts into Account
        json = '{'+
        '    \"accounts\": [{'+
        '        \"uuid\": \"' + ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT + '\",'+
        '        \"company_name\": \"' + ACCOUNT_2_NAME + '\",'+
        '        \"annual_revenue\": ' + ACCOUNT_REVENUE_LOW + ','+
        '        \"number_employees\": 1,'+
        '        \"contacts\": [{'+
        '            \"first_name\": \"John\",'+
        '            \"last_name\": \"Smith\",'+
        '            \"email\": \"john@acme.com\"'+
        '        }, {'+
        '            \"first_name\": \"Maria\",'+
        '            \"last_name\": \"Doe\",'+
        '            \"email\": \"maria@acme.com\"'+
        '        }]'+
        '    }]'+
        '}';
        customInfo = CW_CustomerInfo.parse(json);

        service = new CW_CustomerInfoService();
        responseString = service.updateInsertCustomerInfo(customInfo);
        response = (CW_AbstractRestService.Response)System.JSON.deserialize(responseString, CW_AbstractRestService.Response.class);

        //Check the Response Code
        System.assertEquals(response.responseCode,CW_AbstractRestService.SUCCESS_CODE_200,'The expected response code is not matching');

        //Check that the records has been created and the Process Builder and Formula Fields are working fine
        accounts = accountsSelector.getByCWExternalId(new Set<String>{ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT});
        contacts = contactsSelector.getContactByAccountId(new Set<String>{ACCOUNT_EXTERNAL_ID_NEW_ACCOUNT});
        System.assertEquals(1, accounts.size(), 'The Number of Accounts is not matching');
        System.assertEquals(ACCOUNT_PRIORITY_LOW, accounts.get(0).CW_Priority__c, 'The Account Priority is not matching');
        System.assertEquals(AGENT_NAME_PRIORITY_LOW, accounts.get(0).Owner.Name, 'The Account Owner is not matching');
        System.assertEquals(2, contacts.size(), 'The Number of Contacts related with the Account is not matching');
        System.assertEquals(AGENT_NAME_PRIORITY_LOW, contacts.get(0).Owner.Name, 'The Number of Contacts related with the Account is not matching');
        //END Test Creation of Contacts into Account

        //START Test Update Existing Accounts
        json = '{'+
        '    \"accounts\": [{'+
        '        \"uuid\": \"' + ACCOUNT_EXTERNAL_ID + '\",'+
        '        \"company_name\": \"' + ACCOUNT_NAME + '\",'+
        '        \"annual_revenue\": ' + ACCOUNT_REVENUE_MEDIUM + ','+
        '        \"number_employees\": 8'+
        '    }]'+
        '}';
        customInfo = CW_CustomerInfo.parse(json);

        service = new CW_CustomerInfoService();
        responseString = service.updateInsertCustomerInfo(customInfo);
        response = (CW_AbstractRestService.Response)System.JSON.deserialize(responseString, CW_AbstractRestService.Response.class);

        //Check the Response Code
        System.assertEquals(response.responseCode,CW_AbstractRestService.SUCCESS_CODE_200,'The expected response code is not matching');

        //Check that the records has been created and the Process Builder and Formula Fields are working fine
        accounts = accountsSelector.getByCWExternalId(new Set<String>{ACCOUNT_EXTERNAL_ID});
        contacts = contactsSelector.getContactByAccountId(new Set<String>{ACCOUNT_EXTERNAL_ID});
        System.assertEquals(1, accounts.size(), 'The Number of Accounts is not matching');
        System.assertEquals(ACCOUNT_PRIORITY_MEDIUM, accounts.get(0).CW_Priority__c, 'The Account Priority is not matching');
        System.assertEquals(AGENT_NAME_PRIORITY_MEDIUM, accounts.get(0).Owner.Name, 'The Account Owner is not matching');
        System.assertEquals(0, contacts.size(), 'The Number of Contacts related with the Account is not matching');
        //END Test Update Existing Accounts
    }
}