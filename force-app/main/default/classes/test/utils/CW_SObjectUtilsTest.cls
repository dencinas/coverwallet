/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-22
 * @group Util Test
 * @description Apex Test Class related with CW_SObjectUtils class
 */
@IsTest
public class CW_SObjectUtilsTest {

    /**
     * @description Tries to get the given field value from the given record. Throws exception if the field doesn't exist.
     */
    @IsTest
    private static void getSObjectInnerFieldValue() {
        Account company = new Account(Name='Company Name');
        insert company;

        Account site = new Account(Name='Site Name');
        site.ParentId = company.Id;
        insert site;

        site.Parent = company;

        System.assertEquals('Company Name', CW_SObjectUtils.getSObjectInnerFieldValue('Parent', 'Name', site),'The Account Name doesn´t match');
        try{
            CW_SObjectUtils.getSObjectInnerFieldValue('Parent', 'Fake', site);
        }
        catch(CW_Exceptions.SystemException e){
            System.assert(true);
            return;
        }
        System.assert(true, 'A exception should be thrown');
    }

    /**
     * @description Returns a map with the given records group by the given field. Uses a list to group all records with the same field value.
     */
    @IsTest
    private static void groupRecords() {
        Account company = new Account(Name='Company Name', CW_Id__c='123456-123456');
        insert company;

        Contact contact1 = new Contact(FirstName='FN1', LastName='LN1',Email='email1@email.com');
        Contact contact2 = new Contact(FirstName='FN2', LastName='LN2',Email='email2@email.com');
        contact1.AccountId = company.Id;
        contact1.Account = company;
        contact2.AccountId = company.Id;
        contact2.Account = company;
        List<Contact> contacts = new List<Contact>{contact1, contact2};
        insert contacts;

        Map<String, Map<String, SObject>> accountMapOfMapsOfList = CW_SObjectUtils.groupRecords('Account', 'CW_Id__c', null, 'Email', contacts);
        System.assertEquals(1, accountMapOfMapsOfList.keySet().size(), 'The Map Set shouldn´t be empty');
        System.assertEquals(2,accountMapOfMapsOfList.get('123456-123456').size(), 'The Map Set shouldn´t be empty');
        System.assertEquals('LN2',((Contact)accountMapOfMapsOfList.get('123456-123456').get('email2@email.com')).LastName, 'The Last Name is not matching');
    }
}