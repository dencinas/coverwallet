/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-18
 * @group Dto
 * @description Apex CustomerInfo DTO
 */
public class CW_CustomerInfo {

    /**
     * @description Class the represent Contact Object
     */
	public class Contact {
        /**
         * @description Contact FirstName
         */
        public String first_name {get;set;}
        
        /**
         * @description Contact LastName
         */
        public String last_name {get;set;} 
        
        /**
         * @description Contact Email
         */
        public String email {get;set;}

        /**
         * @description JSONParser Constructor for CW_CustomerInfo.Contact
         */
		public Contact(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'first_name') {
							first_name = parser.getText();
						} else if (text == 'last_name') {
							last_name = parser.getText();
						} else if (text == 'email') {
							email = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Contacts consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
    
    /**
     * @description List of Accounts
     */
	public List<Account> accounts {get;set;} 

    /**
     * @description JSONParser Constructor for CW_CustomerInfo
     */
	public CW_CustomerInfo(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'accounts') {
						accounts = arrayOfAccounts(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'CW_CustomerInfo consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
    
    /**
     * @description Class the represent Account Object
     */
	public class Account {
        /**
         * @description External UUID
         */
        public String uuid {get;set;}
        
        /**
         * @description Company Name
         */
        public String company_name {get;set;} 
        
        /**
         * @description Annual Revenue
         */
        public Integer annual_revenue {get;set;} 
        
        /**
         * @description Number of Employees
         */
        public Integer number_employees {get;set;} 
        
        /**
         * @description List of Contacts
         */
		public List<Contact> contacts {get;set;} 

        /**
         * @description JSONParser Constructor for Account
         */
		public Account(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'uuid') {
							uuid = parser.getText();
						} else if (text == 'company_name') {
							company_name = parser.getText();
						} else if (text == 'annual_revenue') {
							annual_revenue = parser.getIntegerValue();
						} else if (text == 'number_employees') {
							number_employees = parser.getIntegerValue();
						} else if (text == 'contacts') {
							contacts = arrayOfContacts(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Accounts consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
    /**
     * @description Parse to CW_CustomerInfo from String Object
     */
	public static CW_CustomerInfo parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new CW_CustomerInfo(parser);
	}
    
    /**
     * @description Method to consume Unknown Object
     */
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
    
    /**
     * @description Return array of Account from JSONParser Object
     */
    private static List<Account> arrayOfAccounts(System.JSONParser p) {
        List<Account> res = new List<Account>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Account(p));
        }
        return res;
    }

    /**
     * @description Return array of Cotact from JSONParser Object
     */
    private static List<Contact> arrayOfContacts(System.JSONParser p) {
        List<Contact> res = new List<Contact>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Contact(p));
        }
        return res;
    }
}