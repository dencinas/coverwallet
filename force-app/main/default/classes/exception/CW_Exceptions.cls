/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-18
 * @group Exceptions
 * @description Apex CoverWallet Generic Exception
 */
public with sharing class CW_Exceptions {

    //-- EXCEPTION
    /**
     * @description System Exception. Use it to throw unexpected system errors.
     */
    public class SystemException extends Exception { }

    /**
     * @description Service Exception. Use it to throw errors at the Service layer.
     */
    public class ServiceException extends Exception { }
}
