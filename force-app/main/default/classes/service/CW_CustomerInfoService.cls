/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-20
 * @group Service
 * @description Apex Implementation which has the Service Layer for Customer Info
 */
public with sharing class CW_CustomerInfoService {

    /**
     * @description CW_CustomerInfoService Constructor
     */
    public CW_CustomerInfoService() {

    }

    public String updateInsertCustomerInfo(CW_CustomerInfo customerInfo){
        try{
            //Build a Map
            Map<String, Map<String,Contact>> contactsMap = new Map<String, Map<String,Contact>>();
            Map<String, CW_CustomerInfo.Account> accountCustomerInfoMap = new Map<String, CW_CustomerInfo.Account>();
            for (CW_CustomerInfo.Account acc : customerInfo.accounts){
                Map<String,Contact> accContactsMap = new Map<String,Contact>();
                accountCustomerInfoMap.put(acc.uuid, acc);
                
                if (acc.contacts!=null){
                    for(CW_CustomerInfo.Contact cont : acc.contacts){
                        accContactsMap.put(cont.email, new Contact(FirstName = cont.first_name, LastName = cont.last_name, Email = cont.email));
                    }
                    contactsMap.put(acc.uuid, accContactsMap);
                }
            }

            //Query to get Accounts based on the External Ids
            CW_AccountsSelector accountsSelector = new CW_AccountsSelector();
            List<Account> accounts = accountsSelector.getByCWExternalId(accountCustomerInfoMap.keySet());
            //Build Account Map
            Map<String,Account> accountsMap = new Map<String, Account>();
            for(Account acc: accounts){
                accountsMap.put(acc.CW_Id__c, acc);
            }

            //Update Account Information for the Objects
            for (CW_CustomerInfo.Account acc : customerInfo.accounts){
                Id accountId = accountsMap.containsKey(acc.uuid) ? accountsMap.get(acc.uuid).Id : null;
                accountsMap.put(acc.uuid, new Account(Id = accountId, Name = acc.company_name, AnnualRevenue = acc.annual_revenue, NumberOfEmployees = acc.number_employees, CW_Id__c = acc.uuid));
            }

            //Upsert Accounts
            if (Account.SObjectType.getDescribe().isCreateable() && Account.SObjectType.getDescribe().isUpdateable() && !accountsMap.isEmpty()){
                upsert accountsMap.values();
            }

            //Query to get the Contacts based on the Account External Ids
            Map<String, Map<String,SObject>> contactsMapInSalesforce = new Map<String, Map<String,SObject>>();
            CW_ContactsSelector contactsSelector = new CW_ContactsSelector();
            List<Contact> contacts = contactsSelector.getContactByAccountId(accountCustomerInfoMap.keySet());

            contactsMapInSalesforce =   CW_SObjectUtils.groupRecords('Account','CW_Id__c', null, 'Email', contacts);

            List<Contact> contactsToUpdate = new List<Contact>();
            for (CW_CustomerInfo.Account acc : customerInfo.accounts){
                //Merge Contacts
                if (acc.contacts!=null){
                    for(CW_CustomerInfo.Contact cont : acc.contacts){
                        Contact contacto = contactsMap.get(acc.uuid).get(cont.email);
                        contacto.AccountId = accountsMap.get(acc.uuid).Id;
                        if (contactsMapInSalesforce.containsKey(acc.uuid) && contactsMapInSalesforce.get(acc.uuid).containsKey(cont.Email)) {
                            contacto.Id = ((Contact)contactsMapInSalesforce.get(acc.uuid).get(cont.Email)).Id;
                        }
                        contactsToUpdate.add(contacto);
                    }
                }
            }
            //Upsert Contacts
            if (Contact.SObjectType.getDescribe().isCreateable() && Contact.SObjectType.getDescribe().isUpdateable() && !contactsToUpdate.isEmpty()){
                upsert contactsToUpdate;
            }
            
            return JSON.serialize(CW_AbstractRestService.builtResponse(CW_AbstractRestService.SUCCESS_CODE_200,CW_AbstractRestService.MESSAGE_TRANSACTION_PROPERLY_EXECUTED,CW_AbstractRestService.MESSAGE_TRANSACTION_PROPERLY_EXECUTED));
        }
        catch(Exception ex) {
            String responseString = JSON.serialize(CW_AbstractRestService.builtResponse(CW_AbstractRestService.ERROR_CODE_500,CW_AbstractRestService.GENERIC_EXCEPTION_MESSAGE,ex.getMessage()));
            return responseString;
        }
    }
}