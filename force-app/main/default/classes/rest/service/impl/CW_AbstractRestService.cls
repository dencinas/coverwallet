/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-18
 * @group Rest Implementation
 * @description Apex Implementation which has the definition methods of Generic Rest Service.
 */
public with sharing abstract class CW_AbstractRestService {
    /**
     * LIST_STRING_FORMAT
     */
    public static final String LIST_STRING_FORMAT = 'List<{0}>';
    
    /**
     * HEADER_CONTENT_TYPE_ATTRIBUTE
     */
    public static final String HEADER_CONTENT_TYPE_ATTRIBUTE = 'Content-Type';

    /**
     * HEADER_CONTENT_TYPE_VALUE
     */
    public static final String HEADER_CONTENT_TYPE_VALUE = 'application/json';

    /**
     * @description Success code 200 Internal Server Error
     */
    public final static Integer SUCCESS_CODE_200 = 200;

    /**
     * @description Error code 500 Internal Server Error
     */
    public final static Integer ERROR_CODE_500 = 500;

    /**
     * @description Error code 400 Bad Request
     */
    public final static Integer ERROR_CODE_400 = 400;

    /**
     * @description Error code 404 Not Found
     */
    public final static Integer ERROR_CODE_404 = 404;

    /**
     * @description Error code 405 Method Not Allowed
     */
    public final static Integer ERROR_CODE_405 = 405;

    /**
     * @description MESSAGE_TRANSACTION_PROPERLY_EXECUTED
     */
    public static final String MESSAGE_TRANSACTION_PROPERLY_EXECUTED = 'Success Operation';

    /**
     * @description MESSAGE_HTTP_METHOD_NOT_SUPPORTED
     */
    public static final String MESSAGE_HTTP_METHOD_NOT_SUPPORTED = 'Http Method not supported: ';

    /**
     * @description Error message when a dml operation in salesforce fails
     */
    public final static string DMLEXCEPTION_MESSAGE = 'DML Exception';

    /**
     * @description Error message when the callout fails
     */
    public final static string CALLOUTEXCEPTION_MESSAGE ='Callout Exception';

    /**
     * @description Message for the exception when the external info fails
     */
    public final static string EXTERNAL_ID_NOT_FOUND_MESSAGE ='External Id Not Found';

    /**
     * @description Message generic for some exceptions
     */
    public final static string GENERIC_EXCEPTION_MESSAGE ='Generic Exception';

    /**
     * @description Message generic for some exceptions
     */
    public final static string REST_METHOD_DELETE ='DELETE';

    //-- ENUMS
    /**
     * @description Enumerates all valid REST methods.
     */
    public enum RestMethod {
        GET,
        POST,
        PUT,
        PATCH
    }

    /**
     * @description Returns the body of the request.
     * @return body - The body of the request.
     */
    public static String getRequestBody() {
        RestRequest request = RestContext.request; 
        String body = null; 
        if (request != null && request.requestBody != null) { 
            body = (String.isBlank(request.requestBody.toString())) ? null : request.requestBody.toString(); 
        } 
        return body; 
    }

    /**
     * @description Wrapper object to Map the api Response and create the response
     */
    public class Response {
        /**
        * @description Get / Set 'responseCode'.
        */
        public Integer responseCode {get;set;}

        /**
        * @description Get / Set 'message'.
        */
        public String message {get;set;}

        /**
        * @description Get / Set 'messageSalesforce'.
        */
        public String messageSalesforce {get;set;}
    }

    /**
     * @description Returns the body of the request.
     * @param exceptionObject Exception Object to analyze and create the response
     * @param response response to map the HTTP Header status Code
     * @return String - Response JSON serialized
     */
    public static String buildResponseError(Exception exceptionObject, RestResponse response){

        String exceptionType = exceptionObject.getTypeName();

        Response error = new Response();

        switch on exceptionType {
            when 'DmlException', 'CalloutException', 'CW_Exceptions.SystemException' {
                response.statusCode = ERROR_CODE_500;
                error = builtResponse(ERROR_CODE_500,DMLEXCEPTION_MESSAGE,exceptionObject.getMessage());
            }
            when 'ExternalObjectException' {
                response.statusCode = ERROR_CODE_404;
                error = builtResponse(ERROR_CODE_404,EXTERNAL_ID_NOT_FOUND_MESSAGE,exceptionObject.getMessage());
            }
            when 'CW_Exceptiona.ServiceException' {
                response.statusCode = ERROR_CODE_405;
                error = builtResponse(ERROR_CODE_405,MESSAGE_HTTP_METHOD_NOT_SUPPORTED,exceptionObject.getMessage());
            }
            when else {
                response.statusCode = ERROR_CODE_500;
                error = builtResponse(ERROR_CODE_500,GENERIC_EXCEPTION_MESSAGE,exceptionObject.getMessage());
            }
        }

        String responseString = JSON.serialize(error);

        return responseString;
    }

    /**
    * @description return the response with information given in parameters
    * @param  code the number of the error
    * @param  message errorMessage
    * @param  getMessage   getMessage from the exception
    * @return the response record with the given info
    */ 
    public static Response builtResponse(Integer code, String message, String getMessage){
        Response response = new Response();
        response.responseCode = code;
        response.message = message;
        response.messageSalesforce = getMessage;
        return response;
    }
}
