/**
 * @author David Encinas Gutierrez david.encinas.gutierrez@gmail.com
 * @date 2020-12-18
 * @group Rest Implementation
 * @description Apex Implementation which has the definition methods of CustomerInfo Rest Service.
 */
@RestResource(urlMapping='/api/1.0.0/CustomerInfo/*')
global class CW_CustomerInfoRestService extends CW_AbstractRestService{

    //-- SERVICE
    /**
     * @description Rest Service instance
     */
    private static CW_CustomerInfoRestService restService = new CW_CustomerInfoRestService();

    //-- METHODS
    /**
     * @description Method that is involved when GET operations and return the Result
     */
    @HttpGet
    global static void genericGet() {
        //restService.processRequest(CW_AbstractRestService.RestMethod.GET.name(), 200);
    }
    
    /**
     * @description Method that is involved when POST operations and return the Result
     */
    @HttpPost
    global static void genericPost() {
        restService.processRequest(CW_AbstractRestService.RestMethod.POST.name(), 200);
    }

    /**
     * @description Method that is involved when PATCH operations and return the Result
     */
    @HttpPatch
    global static void genericPatch() {
        //restService.processRequest(CW_AbstractRestService.RestMethod.PATCH.name(), 201);
    }
    
    /**
     * @description Method that is involved when DELETE operations and return the Result
     */
    @HttpDelete
    global static void genericDelete() {
        //restService.processRequest(CW_AbstractRestService.REST_METHOD_DELETE, 202);
    }


    /**
     * @description Method that invoke the doGet, doPost, doPatch and doDelete actions depending of the processType Param and controll the response on success and Exceptions
     * @param processType HTTP Method to process
     * @param codeSuccess Code that return if all the process finally correctly
     */
    public void processRequest(String processType, Integer codeSuccess){
        RestResponse response = RestContext.response;
        response.addHeader(HEADER_CONTENT_TYPE_ATTRIBUTE,HEADER_CONTENT_TYPE_VALUE);
        String responseString;
        try{
            switch on processType.toUpperCase() {
                /*when 'GET' {
                    //TODO Invoke Get Method
                    throw new CW_Exceptions.ServiceException(MESSAGE_HTTP_METHOD_NOT_SUPPORTED + processType);
                }*/
                when 'POST' {
                    responseString = this.doPost();
                }
				/*
                when 'PATCH' {
                    //TODO Invoke PATH Method
                    throw new CW_Exceptions.ServiceException(MESSAGE_HTTP_METHOD_NOT_SUPPORTED + processType);
                }
                when 'DELETE' {
                    //TODO Invoke Delete Method
                    throw new CW_Exceptions.ServiceException(MESSAGE_HTTP_METHOD_NOT_SUPPORTED + processType);
                }*/
                when else {
                    throw new CW_Exceptions.ServiceException(MESSAGE_HTTP_METHOD_NOT_SUPPORTED + processType);
                }
            }
            response.statusCode = codeSuccess;
        } catch(Exception exceptionObject){
            responseString = buildResponseError(exceptionObject, response);
        }
        response.responseBody = Blob.valueOf(responseString);
    }

    /**
     * @description Method that contains the rest context and performs the necessary operations to Create sObjects
     * @return JSON format with the result of the Post operation
     */
    public String doPost() {
        CW_CustomerInfo customerInfo = (CW_CustomerInfo) JSON.deserialize(getRequestBody(), CW_CustomerInfo.class);

        CW_CustomerInfoService service = new CW_CustomerInfoService();
        String result = service.updateInsertCustomerInfo(customerInfo);
        return result;
    }
}